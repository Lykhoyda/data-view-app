import React, {Component} from 'react'
import {connect} from 'react-redux'
import styled from 'styled-components'

import Select from '../../components/Select'
import DataTable from '../../components/DataTable'
import {fetchAnalyzeParameters, fetchDataForTables} from './dataReducers'

const Wrapper = styled.div`
  padding: 0 20px;
  max-width: 1024px;
  margin: auto;
`

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 20px;
`

class DataVisualize extends Component {
  componentDidMount () {
    this.props.fetchAnalyzeParameters()
  }

  render () {
    return (
      <Wrapper>
          <Select 
            labelText='Select data to analyze'
            containerStyle='select-bar__wrapper'
            onSelectChange={this.props.fetchDataForTables}
            tableNames={this.props.dataVisualize.visualizeParameters} />
          <ContentWrapper>
              <DataTable 
                tableHeading={this.props.dataVisualize.currentParameter}
                tableData={this.props.dataVisualize.tableData}/>
          </ContentWrapper>
      </Wrapper>
    )
  }
}

export default connect(
  (state) => ({dataVisualize: state.dataVisualize}),
  {fetchAnalyzeParameters, fetchDataForTables}
)(DataVisualize)