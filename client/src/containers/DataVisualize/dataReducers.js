import { getAnalyzeParameters, getDataForTabel, getParameter } from '../../lib/dataServices'

export const ANALYZE_PARAMETERS_LOAD = 'ANALYZE_PARAMETERS_LOAD'
export const TABLE_DATA_LOAD = 'TABLE_DATA_LOAD'
export const SET_CURRENT_PARAMETER = 'SET_CURRENT_PARAMETER'

const initState = {
  visualizeParameters: [],
  tableData: [],
  currentParameter: ''
}


// Actions
export const loadAnalyzeParameters = (analyzeParameters) => ({type: ANALYZE_PARAMETERS_LOAD, payload: analyzeParameters})
export const loadDataCurrentTable  = (tableData) => ({type: TABLE_DATA_LOAD, payload: tableData})
export const setCurrentParameter = (parameter) => ({type: SET_CURRENT_PARAMETER, payload: parameter})

export const fetchAnalyzeParameters = () => {
  return (dispatch) => {
    getAnalyzeParameters()
      .then(analyzeParameters => {
        const tableNames = analyzeParameters
          .map(item => ({id: item.id, name: item.name}))
          .filter(item => item.name !== 'age')

        return dispatch(loadAnalyzeParameters(tableNames))
    })
  }
}

export const fetchDataForTables = (id) => {
  return (dispatch) => {
    getParameter(id).then(res => {
      return dispatch(setCurrentParameter(res))
    })
    getDataForTabel(id).then(res => {
      return dispatch(loadDataCurrentTable(res))
    })
  }
}

export default (state=initState, action) => {
  switch (action.type) {
    case ANALYZE_PARAMETERS_LOAD:
      return {...state, visualizeParameters: action.payload}
    case TABLE_DATA_LOAD:
      return {...state, tableData: action.payload}
    case SET_CURRENT_PARAMETER:
      return {...state, currentParameter: action.payload}
    default:
      return state
  }
}