import React, {Component} from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import styled, { injectGlobal } from 'styled-components'

import AppBar from '../../components/AppBar'
import DataVisualize from '../DataVisualize'

injectGlobal`
  body {
    margin: 0;
    padding: 0;
  }
`
const AppWrapper = styled.div`
  padding: 0;
  margin: 0;
`

class App extends Component {
  render () {
    return (
      <MuiThemeProvider>
        <AppWrapper>
          <header>
            <AppBar />
          </header>
          <DataVisualize />
        </AppWrapper>
      </MuiThemeProvider>
    )
  }
}

export default App