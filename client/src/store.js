import {createStore, applyMiddleware, combineReducers} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import DataVisualizeReducers from './containers/DataVisualize/dataReducers'

const reducer = combineReducers({
  dataVisualize: DataVisualizeReducers
})

export default createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(thunk)
  )
)