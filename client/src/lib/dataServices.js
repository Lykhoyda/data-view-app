const baseUrl = process.env.REACT_APP_BASE_URL

export const getAnalyzeParameters = () => {
  return fetch(baseUrl, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(res => res.json())
}

export const getDataForTabel = (id) => {
  return fetch(`${baseUrl}/tableData/${id}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(res => res.json())
}

export const getParameter = (id) => {
  return fetch(`${baseUrl}/parameter/${id}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(res => res.json())
}