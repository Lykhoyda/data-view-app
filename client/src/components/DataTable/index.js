import React, {Component} from 'react'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table'

class TableData extends Component {
  renderTableRow = () => {
    return this.props.tableData.map(item => {
      return (
        <TableRow>
          <TableRowColumn>{item.field}</TableRowColumn>
          <TableRowColumn>{item.count}</TableRowColumn>
          <TableRowColumn>{item.average}</TableRowColumn>
        </TableRow>
      )
    })
  }

  render () {
    return (
      this.props.tableData.length ? 
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
                <TableHeaderColumn 
                  colSpan="3" 
                  style={{textAlign: 'center', fontSize: '18px', textTransform: 'capitalize'}}>
                  {this.props.tableHeading}
                </TableHeaderColumn>
              </TableRow>
            <TableRow>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>Count</TableHeaderColumn>
              <TableHeaderColumn>Average</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {this.renderTableRow()}
          </TableBody>
        </Table>
      : null
    )
  }
}

export default TableData