import React, {Component} from 'react'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'

class Select extends Component {
  state = {
    value: 'Select parameter',
  }

  handleChange = (event, index, value) => {
    this.props.onSelectChange(value)
    this.setState({value})
  }

  renderItems = () => {
    return this.props.tableNames.map( (item, index) => {
        return <MenuItem value={item.id} key={index} primaryText={item.name} />
    })
  }

  render() {
    const { tableNames } = this.props

    return (
      <div className={this.props.containerStyle}>
        <SelectField
          listStyle={{textTransform: 'capitalize'}}
          labelStyle={{textTransform: 'capitalize'}}
          floatingLabelText={this.props.labelText}
          value={this.state.value}
          onChange={this.handleChange}
          maxHeight={200}
          fullWidth={true}
        >
        {tableNames ? this.renderItems() : ''}
        </SelectField>
      </div>
    )
  }
}

export default Select