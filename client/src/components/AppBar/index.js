import React, {Component} from 'react'
import AppBar from 'material-ui/AppBar'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import NavigationClose from 'material-ui/svg-icons/navigation/close'

class AppBarExampleIcon extends Component {
  state = {
    open: false
  }

  handleToggle = () => this.setState({open: !this.state.open});

  render () {
    return (
      <div className="AppBarWrapper">
        <AppBar
          title="Data view App"
          iconElementLeft={this.state.open ? <IconButton><NavigationClose /></IconButton> : null}
          iconClassNameRight="muidocs-icon-navigation-expand-more"
          onLeftIconButtonTouchTap={this.handleToggle}
        />
        <Drawer
          docked={false}
          onRequestChange={(open) => this.setState({open})}
          open={this.state.open}>
          <MenuItem><a href="https://github.com/Lykhoyda/">Github</a></MenuItem>
          <MenuItem><a href="https://bitbucket.org/Lykhoyda/data-view-app">Bitbucket</a></MenuItem>
        </Drawer>
      </div>
    )
  }
}

export default AppBarExampleIcon;