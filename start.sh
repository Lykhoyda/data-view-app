#!/usr/bin/env bash

# Run the server
cd ./server/
npm install
npm start

# Run client
cd ../client/
npm install
npm start