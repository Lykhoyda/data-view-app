'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _DataApp = require('./controller/DataApp');

var _DataApp2 = _interopRequireDefault(_DataApp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = (0, _express2.default)();
router.use('/data-app', (0, _DataApp2.default)());

exports.default = router;
//# sourceMappingURL=routes.js.map