'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _db = require('../db');

(0, _db.initializeDb)(_db.db);

exports.default = function () {
  var api = (0, _express.Router)();
  var mainTable = 'census_learn_sql';

  // GET COLUMN NAMES 
  api.get('/', function (req, res) {
    _db.db.all('SELECT id, name from parameters', function (err, rows) {
      if (err) {
        res.send(err);
      }
      res.json(rows);
    });
  });

  api.get('/parameter/:id', function (req, res) {
    var id = req.params.id;

    _db.db.all('SELECT * FROM parameters\n      WHERE id = (SELECT id FROM parameters WHERE id = ' + id + ')', function (err, rows) {
      if (err) {
        res.send(err);
      }
      res.json(rows[0].name);
    });
  });

  // GET DATA FOR CURRENT PARAMETER
  api.get('/tableData/:id', function (req, res) {
    var id = req.params.id;

    function getData(id, callback) {
      _db.db.all('SELECT * FROM parameters\n        WHERE id = (SELECT id FROM parameters WHERE id = ' + id + ')', function (err, rows) {
        if (err) {
          res.send(err);
        }
        callback(rows[0].name);
      });
    }

    getData(id, function (nameByid) {
      _db.db.all('SELECT DISTINCT\n        "' + nameByid + '" as field,\n        count(age) as count,\n        AVG(age) as average\n      FROM ' + mainTable + ' WHERE "' + nameByid + '" IS NOT NULL GROUP BY "' + nameByid + '"', function (err, rows) {
        if (err) {
          res.send(err);
        }
        res.json(rows);
      });
    });
  });

  return api;
};
//# sourceMappingURL=DataApp.js.map