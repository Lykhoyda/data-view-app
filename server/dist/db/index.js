'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.db = undefined;
exports.initializeDb = initializeDb;

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var sqlite3 = require('sqlite3').verbose();
var dbPath = _path2.default.resolve(__dirname, './us-census.db');

var db = exports.db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE, function (err) {
  err ? console.error(err.message) : console.log('Connected to the in-memory SQlite database.');
});

function initializeDb() {
  db.serialize(function () {
    db.run("DROP TABLE IF EXISTS parameters");
    db.run("CREATE TABLE parameters (id INTEGER, name TEXT)");
    db.all('PRAGMA table_info(census_learn_sql)', function (err, rows) {
      var stmt = db.prepare("INSERT INTO parameters VALUES (?, ?)");
      rows.map(function (item) {
        stmt.run(item.cid, item.name);
      });
      stmt.finalize();
    });
  });
}
//# sourceMappingURL=index.js.map