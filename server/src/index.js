
import http from 'http'
import express from 'express'
import bodyParser from 'body-parser'
import config from './config'
import routes from './routes'
import cors from 'cors'

const app = express()
app.use(cors())
app.server = http.createServer(app)

app.use(bodyParser.json({
    limit: config.bodyLimit
}))

app.use('/', routes)
app.server.listen(config.port)

export default app

