import { Router } from 'express';
import {db, initializeDb} from '../db'

initializeDb(db)

export default () => {
  const api = Router()
  const mainTable = 'census_learn_sql'

  // GET COLUMN NAMES 
  api.get('/', (req, res) => {
    db.all(`SELECT id, name from parameters`, (err, rows) => {
      if (err) { res.send(err) }
      res.json(rows)
    })
  })

  api.get('/parameter/:id', (req, res) => {
    const id = req.params.id
    
    db.all(`SELECT * FROM parameters
      WHERE id = (SELECT id FROM parameters WHERE id = ${id})`, (err, rows) => {
      if (err) { res.send(err)}
      res.json(rows[0].name)
    })
  })

  // GET DATA FOR CURRENT PARAMETER
  api.get('/tableData/:id', (req, res) => {
    const id = req.params.id

    function getData(id, callback) {
      db.all(`SELECT * FROM parameters
        WHERE id = (SELECT id FROM parameters WHERE id = ${id})`, (err, rows) => {
        if (err) { res.send(err)}
        callback(rows[0].name)
      })
    }
    
    getData(id, function(nameByid) {
      db.all(`SELECT DISTINCT
        "${nameByid}" as field,
        count(age) as count,
        AVG(age) as average
      FROM ${mainTable} WHERE "${nameByid}" IS NOT NULL GROUP BY "${nameByid}"`, 
        (err, rows) => {
        if (err) { res.send(err) }
        res.json(rows)
      })
    })
  })
  
  return api
}