import express from 'express'
import DataApp from './controller/DataApp'

const router = express();
router.use('/data-app', DataApp())

export default router