import path from 'path'
const sqlite3 = require('sqlite3').verbose()
const dbPath = path.resolve(__dirname, './us-census.db')

export const db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE, err => {
    err ? console.error(err.message) : console.log('Connected to the in-memory SQlite database.')
})

export function initializeDb () {
  db.serialize(() => {
    db.run("DROP TABLE IF EXISTS parameters")
    db.run("CREATE TABLE parameters (id INTEGER, name TEXT)")
    db.all(`PRAGMA table_info(census_learn_sql)`, (err, rows) => {
      const stmt = db.prepare("INSERT INTO parameters VALUES (?, ?)")
      rows.map(item => {
        stmt.run(item.cid, item.name);
      })
      stmt.finalize()
    })
  })
}
